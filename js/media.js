jQuery(document).ready(function () {

    var $ = jQuery;

    if ($('.set_custom_images').length > 0) {


        var media_uploader = null;
        var libr = null;

        function open_media_uploader_gallery(th) {
            media_uploader = wp.media({
                frame: "post",
                state: "gallery-edit",
                multiple: true
            });


            media_uploader.on("update", function () {
                var length = media_uploader.state().attributes.library.length;
                var images = media_uploader.state().attributes.library.models;

                console.log(images);

                var arr = [];

                images.forEach(function (item, i, ar) {
                    arr[arr.length] = item.id;
                });


//                console.log(media_uploader.state().attributes.library);
                var form = $(th).parents('form');



                $(".sd_actor_gallery").val(JSON.stringify(arr));

                $.post(ajaxurl, form.serialize() + "&action=sdShowGallery" , function (resp) {

                    $("#sd_gallery_images").html(resp);


                });


                //       $(".sd_actor_gallery").val(JSON.stringify(media_uploader.state().attributes.library));

            });

            media_uploader.open();
        }


        if (typeof wp !== 'undefined' && wp.media) {

            $(document).on('click', '.set_custom_images', function (e) {

                e.preventDefault();

                open_media_uploader_gallery(this);

                return false;
            });
        }
    }


});