<?php
/*
Plugin Name: Schauspieler Datenbank
Version:     1.0
Author:      n-size services GmbH
License:     
License URI: 
Domain Path: /languages/
*/

/**
 * Protect direct access
 */

if ( ! defined( 'ABSPATH' ) ) die( 'Accessing this file directly is denied.' );



if (!class_exists('Schauspieler_Datenbank')){
    class Schauspieler_Datenbank{
        /**
         * Network_Partner constructor.
         */
        public function __construct()
        {
            //At first, we need to define the required CONSTANT for the plugin
            $this->_define_constant();

            // now lets include all the required files
            $this->_include();

			// activate plugin settings
			// Create an own Class

            // register custom post and custom metabox
            // Create an own Class - here is an example: new NW_PosttypeUtility(); -> see also includes/sd-posttypeutility.php

            // Modify the text of the feature image meta box on our custom post page

//            new LCP_Featured_Img_Customizer(array(
//                'post_type'     => 'networkpartner',
//                'metabox_title' => __( 'Logo', NW_TEXTDOMAIN ),
//                'set_text'      => __( 'Set logo', NW_TEXTDOMAIN ),
//                'remove_text'   => __( 'Remove logo', NW_TEXTDOMAIN )
//            ));

            // register shorcodes
            new SD_Actor_Entry_Shortcode();
            new SD_Actor_Overview_Shortcode();


            // enqueue all the required styles and scripts
            // the best hook to enqueue scripts and style for the front end is the template redirect hook as said by brad william, author of professional plugin development
            add_action( 'template_redirect', array($this, 'frontend_enqueue_scripts_and_styles') );
            add_action('admin_enqueue_scripts', array($this, 'backend_enqueue_scripts_and_styles'));


            // add usage and support menu to the admin menu
            add_action('admin_menu', array($this, 'hook_usage_and_support_submenu'));



            // Miscellaneous features
            // Enables shortcode for the Text Widget
            add_filter('widget_text', 'do_shortcode');



            // activate, deactivation uninstall plugin functions
            register_activation_hook(__FILE__, array('SD_Install_Uninstall', 'activate_plugin'));
            register_deactivation_hook(__FILE__, array('SD_Install_Uninstall','deactivation_plugin'));
            register_uninstall_hook(__FILE__, array('SD_Install_Uninstall','uninstall_plugin'));
        }


        /**
         * Enqueues all the styles and the scripts required by the plugin
         */
        public function frontend_enqueue_scripts_and_styles()
        {
            wp_enqueue_style(SD_POST_TYPE, plugin_dir_url(__FILE__) . 'css/sd-public.css');
        }

        /**
         *It enqueues the scripts and styles for the backend operation of the plugin
         * @return void
         */
        public function backend_enqueue_scripts_and_styles()
        {
            global $typenow;
            // enqueue scripts and style only on our plugin page
            if ( ( $typenow === SD_POST_TYPE) ) {

                wp_enqueue_style(SD_POST_TYPE, plugin_dir_url(__FILE__) . 'css/sd-admin-panel.css');

                wp_enqueue_script( 'user_register-plugin-admin-script', plugin_dir_url(__FILE__) . '/js/media.js', array('jquery') );

            }
        }
        /**
         *It defines essential CONSTANT for the plugin
         * @return void
         */
        private function _define_constant()
        {
            /**
             * Defining constants
             */
            if( ! defined( 'SD_PLUGIN_DIR' ) ) define( 'SD_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
            if( ! defined( 'SD_PLUGIN_URI' ) ) define( 'SD_PLUGIN_URI', plugins_url( '', __FILE__ ) );
            if( ! defined( 'SD_TEXTDOMAIN' ) ) define( 'SD_TEXTDOMAIN', 'schauspieler-datenbank' );

            if( ! defined( 'SD_POST_TYPE' ) ) define( 'SD_POST_TYPE', 'sd_actor' );
            if( ! defined( 'SD_MOVIES_TAX' ) ) define( 'SD_MOVIES_TAX', 'sd-actor-movies' );
            if( ! defined( 'SD_AWARDS_TAX' ) ) define( 'SD_AWARDS_TAX', 'sd-actor-awards' );

            if( ! defined( 'SD_SETTING_OPTION_NAME' ) ) define( 'SD_SETTING_OPTION_NAME', 'sd_setting_option' );

            if( ! defined( 'SD_ACTOR_OVERVIEW_TAG' ) ) define( 'SD_ACTOR_OVERVIEW_TAG', 'actor-overview' );
            if( ! defined( 'SD_ACTOR_ENTRY_TAG' ) ) define( 'SD_ACTOR_ENTRY_TAG', 'actor' );
			// ... add the constants here
        }

        /**
         *It includes all the required files for the plugin
         * @return void
         */
        private function _include()
        {
            require_once SD_PLUGIN_DIR . 'includes/sd-install-uninstall.php';
            require_once SD_PLUGIN_DIR . 'includes/sd-actor-overview-shortcode.php';
            require_once SD_PLUGIN_DIR . 'includes/sd-actor-entry-shortcode.php';
            require_once SD_PLUGIN_DIR . 'includes/sd-posttypeutility.php';

			// ... add the other files here
        }


        /**
         * It adds the Usage and Support submenu page to the admin menu
         * @return void
         */
        function hook_usage_and_support_submenu() {
            add_submenu_page(
                'edit.php?post_type='.SD_POST_TYPE,
                __('Usage & Support', SD_TEXTDOMAIN),
                __('Usage & Support', SD_TEXTDOMAIN),
                'manage_options',
                'sd_usage_support',
                array($this, 'sd_display_usage_and_support')
            );
        }


        /**
         * It displays content for the usage and support screen of the plugin
         */
        public function sd_display_usage_and_support()
        {
            // include the file that contains the usage and support
            include SD_PLUGIN_DIR .'includes/view/sd-usage-support.php';
        }

    } // end of Network_Partner class

} // end if



// get the plugin running
new Schauspieler_Datenbank();
new SD_PosttypeUtility();
