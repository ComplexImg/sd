<?php

/**
 * Protect direct access
 */
if (!defined('ABSPATH')) die('Accessing this file directly is denied.');

if (!class_exists("SD_Actor_Overview_Shortcode")) {
    class SD_Actor_Overview_Shortcode
    {
        public function __construct()
        {
            add_shortcode(SD_ACTOR_OVERVIEW_TAG, array($this, 'output_shortcode'));
        }


        public function output_shortcode($atts)
        {
            $params = shortcode_atts(array(
                'columns' => 3,
                'awards' => null,
                'movies' => null,
            ), $atts);


            $query = array(
                'post_type' => SD_POST_TYPE,
                'posts_per_page' => -1,
            );


            $queryParams = array();

            if ($params['awards'] != null) {
                $queryParams = array_merge(
                    $queryParams,
                    array(
                        SD_AWARDS_TAX => explode(",", $params['awards'])
                    )
                );
            }

            if ($params['movies'] != null) {
                $queryParams = array_merge(
                    $queryParams,
                    array(
                        SD_MOVIES_TAX => explode(",", $params['movies'])
                    )
                );
            }

            $width = (int)(100 / $params['columns']);

            $query = array_merge(
                $query,
                $queryParams
            );

            $actors = new WP_Query($query);

            global $post;

            ob_start();

            echo "<div class=\"sd_schortcodes_container\">
                <ul class=\"sd_schortcodes_box\">
            ";


            while ($actors->have_posts()) :
                $actors->the_post();
                $link = get_permalink($post->ID);
                $img_url = get_the_post_thumbnail_url();
                $name = get_the_title();
                $url = " onclick=\"window.open('{$link}')\"";
                // the_post_thumbnail(array(150,150));
                ?>
                <li class="sd_block" style="max-width: <?php echo $width?>%; min-width: 200px">
                    <div class="sd_table_entry_padding" <?php echo $url?>>
                        <img src='<?php echo $img_url ?>' />
                            <?php echo  $name  ?>
                    </div>
                </li>
                <?php

            endwhile;

            echo "
                </ul>
            </div>";
            wp_reset_postdata();

            $output_string = ob_get_contents();
            ob_end_clean();
            return $output_string;

        }
    }
}


