<?php
/**
 * Protect direct access
 */
if (!defined('ABSPATH')) die('Accessing this file directly is denied.');


if (!class_exists('SD_Install_Uninstall')) {

    class SD_Install_Uninstall
    {

        public static function activate_plugin()
        {

        }

        public static function deactivation_plugin()
        {

        }


        public static function uninstall_plugin()
        {

        }


    }
}