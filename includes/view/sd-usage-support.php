<div class="sd_support_wrapper">
    <div class="left">
        <div class="postbox">
            <h2>Usage</h2>
            <p>1. Add New Logo.</p>
            <p>2. Customize the settings</p>
            <p>3. Use the shortcode</p>
            <br>

            <h2>Shortcodes</h2>
            <p>1. Actor entry <br/><br/>
                <code><strong>[actor entry=1]</strong></code> or <code><strong>[actor entry="1,2,3"]</strong></code></p>
            <p>
                <br/>
                2. Actor overview <br/><br/>
                <code><strong>[actor-overview columns=3]</strong></code>  - echo all<br/><br/>
                <code><strong>[actor-overview columns=2 movies="Term 2 , Term 3"]</strong></code>  - echo actors with this movies<br/><br/>
                <code><strong>[actor-overview columns=1 movies="Term 2"]</strong></code>  - echo actors with this movie<br/><br/>
                <code><strong>[actor-overview columns=4 movies="Term 2" awards="Oscar"]</strong></code> - echo actors with this movie AND this award<br/><br/>
                <code><strong>[actor-overview columns=3 movies="Term 2, Term 3" awards="Oscar, Golden Globes"]</strong></code>  - echo actors with this movies AND this awards<br/>

            </p>
            <br>
        </div>
    </div>
    <div class="right">

    </div>
</div>

