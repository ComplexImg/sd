<div class="sd_post_content">
    <table>
        <?php if ($sd_actor_country) : ?>
            <tr>
                <td>
                    <?php esc_html_e('Country: ', SD_TEXTDOMAIN); ?>
                </td>
                <td>
                    <?php echo $sd_actor_country ?>
                </td>
            </tr>
        <?php endif;

        if ($sd_actor_common) : ?>
            <tr>
                <td>
                    <?php esc_html_e('Common: ', SD_TEXTDOMAIN); ?>
                </td>
                <td>
                    <?php echo $sd_actor_common ?>
                </td>
            </tr>
        <?php endif;

        if ($sd_eye_color) : ?>
            <tr>
                <td>
                    <?php esc_html_e('Eye color: ', SD_TEXTDOMAIN); ?>
                </td>
                <td>
                    <?php echo $sd_eye_color ?>
                </td>
            </tr>
        <?php endif;

        if ($sd_actor_hair) : ?>
            <tr>
                <td>
                    <?php esc_html_e('Hair: ', SD_TEXTDOMAIN); ?>
                </td>
                <td>
                    <?php echo $sd_actor_hair ?>
                </td>
            </tr>
        <?php endif;

        if ($sd_actor_size) : ?>
            <tr>
                <td>
                    <?php esc_html_e('Size: ', SD_TEXTDOMAIN); ?>
                </td>
                <td>
                    <?php echo $sd_actor_size ?>
                </td>
            </tr>
        <?php endif;

        if ($sd_actor_weight) : ?>
            <tr>
                <td>
                    <?php esc_html_e('Weight: ', SD_TEXTDOMAIN); ?>
                </td>
                <td>
                    <?php echo $sd_actor_weight ?>
                </td>
            </tr>
        <?php endif;

        if ($sd_actor_instagram) : ?>
            <tr>
                <td>
                    <?php esc_html_e('Instagram: ', SD_TEXTDOMAIN); ?>
                </td>
                <td>
                    <a href="<?php echo $sd_actor_instagram ?>"><?php echo $sd_actor_instagram ?></a>
                </td>
            </tr>
        <?php endif;

        if ($sd_actor_twitter) : ?>
            <tr>
                <td>
                    <?php esc_html_e('Twitter: ', SD_TEXTDOMAIN); ?>
                </td>
                <td>
                    <a href="<?php echo $sd_actor_twitter ?>"><?php echo $sd_actor_twitter ?></a>
                </td>
            </tr>
        <?php endif; ?>
    </table>

    <?php if ($sd_post_excerpt) : ?>
        <div>
            <h3><?php _e('Actor short info', SD_TEXTDOMAIN); ?></h3>
            <?php echo $sd_post_excerpt ?>
        </div>
    <?php endif; ?>

    <?php if ($sd_actor_life) : ?>
        <div>
            <h3><?php _e('Actor life', SD_TEXTDOMAIN); ?></h3>
            <?php echo $sd_actor_life ?>
        </div>
    <?php endif; ?>

    <?php if (count($sd_actor_awards)) : ?>
        <div>
            <h3><?php _e('Actor awards', SD_TEXTDOMAIN); ?></h3>
            <?php
            foreach ($sd_actor_awards as $award) {
                echo "<a href='" . get_term_link($award) . "'>{$award->name}</a><br/>";
            }
            ?>
        </div>
    <?php endif; ?>

    <?php if (count($sd_actor_movies)) : ?>
        <div>
            <h3><?php _e('Actor movies', SD_TEXTDOMAIN); ?></h3>
            <?php
            foreach ($sd_actor_movies as $movie) {
                echo "<a href='" . get_term_link($movie) . "'>{$movie->name}</a><br/>";
            }
            ?>
        </div>
    <?php endif; ?>

    <?php if ($sd_actor_gallery) : ?>
        <div>
            <h3><?php _e('Actor gallery', SD_TEXTDOMAIN); ?></h3>
            <?php
            echo $sd_actor_gallery;
            ?>
        </div>
    <?php endif; ?>
</div>
