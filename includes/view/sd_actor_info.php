<table class="sd_actors_info_table">
    <tr>
        <td>
            <?php esc_html_e('Country: ', SD_TEXTDOMAIN); ?>
        </td>
        <td><input type="text"
                   name="sd_actor_country"
                   id="sd_actor_country"
                   value="<?php echo esc_attr($sd_actor_country); ?>"
                   maxlength="50"/>
        </td>
    </tr>
    <tr>
        <td>
            <?php esc_html_e('Common: ', SD_TEXTDOMAIN); ?>
        </td>
        <td><input type="text"
                   name="sd_actor_common"
                   id="sd_actor_common"
                   value="<?php echo esc_attr($sd_actor_common); ?>"
                   maxlength="50"/>
        </td>
    </tr>
    <tr>
        <td>
            <?php esc_html_e('Eye color: ', SD_TEXTDOMAIN); ?>
        </td>
        <td><input type="text"
                   name="sd_eye_color"
                   id="sd_eye_color"
                   value="<?php echo esc_attr($sd_eye_color); ?>"
                   maxlength="20"/>
        </td>
    </tr>
    <tr>
        <td>
            <?php esc_html_e('Hair: ', SD_TEXTDOMAIN); ?>
        </td>
        <td><input type="text"
                   name="sd_actor_hair"
                   id="sd_actor_hair"
                   value="<?php echo esc_attr($sd_actor_hair); ?>"
                   maxlength="20"/>
        </td>
    </tr>
    <tr>
        <td>
            <?php esc_html_e('Size: ', SD_TEXTDOMAIN); ?>
        </td>
        <td><input type="text"
                   name="sd_actor_size"
                   id="sd_actor_size"
                   value="<?php echo esc_attr($sd_actor_size); ?>"
                   maxlength="3"/>
            <?php esc_html_e('cm', SD_TEXTDOMAIN); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php esc_html_e('Weight: ', SD_TEXTDOMAIN); ?>
        </td>
        <td><input type="text"
                   name="sd_actor_weight"
                   id="sd_actor_weight"
                   value="<?php echo esc_attr($sd_actor_weight); ?>"
                   maxlength="3"/>
            <?php esc_html_e('kg', SD_TEXTDOMAIN); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php esc_html_e('Instagram: ', SD_TEXTDOMAIN); ?>
        </td>
        <td><input type="text"
                   name="sd_actor_instagram"
                   id="sd_actor_instagram"
                   value="<?php echo esc_attr($sd_actor_instagram); ?>"
                   maxlength="100"/>
        </td>
    </tr>
    <tr>
        <td>
            <?php esc_html_e('Twitter: ', SD_TEXTDOMAIN); ?>
        </td>
        <td><input type="text"
                   name="sd_actor_twitter"
                   id="sd_actor_twitter"
                   value="<?php echo esc_attr($sd_actor_twitter); ?>"
                   maxlength="100"/>
        </td>
    </tr>
</table>