<?php
if (!defined('ABSPATH')) die('Direct access is not allowed');

if (!class_exists('SD_PosttypeUtility')) {
    /**
     * This utility class helps creating custom post and custom meta box
     * Class NW_PosttypeUtility
     */
    class SD_PosttypeUtility
    {

        public function __construct()
        {
            // create custom post type
            add_action('init', array($this, 'register_actor_post'));

            // create actor_awards and actor_movies
            add_action('init', array($this, 'register_actor_taxonomies'));

            // add metaboxes
            add_action('do_meta_boxes', array($this, 'add_meta_boxes'));

            // save  content of meta box.
            add_action('save_post', array($this, 'save_meta_box_data'));

            // add new columns (id, img)
            add_filter('manage_posts_columns', array($this, 'nwpost_add_id_column'), 10, 2);
            add_action('manage_posts_custom_column', array($this, 'nwpost_id_column_content'), 10, 1);

            // add hook for sort by id
            add_filter("manage_edit-" . SD_POST_TYPE . "_sortable_columns", array($this, 'logos_sortable_columns'));

            add_filter("the_content", array($this, 'add_info_to_post'), 1);


            add_action('wp_ajax_sdShowGallery', array($this, "ajaxShowGallery"));


            add_action("admin_enqueue_scripts", array($this, "enqueue_media_uploader"));

        }


        function enqueue_media_uploader()
        {
            wp_enqueue_media();
        }

        function ajaxShowGallery()
        {

            if (!empty($_POST) && check_admin_referer('sslp_post_nonce', 'sslp_add_edit_staff_member_noncename')) {

                $imgs = json_decode($_POST['sd_actor_gallery'], false);

                $sd_actor_gallery = gallery_shortcode(array(
                    "ids" => $imgs
                ));

                echo $sd_actor_gallery;

            }
            wp_die();
        }


        // add to post all meta data
        function add_info_to_post($content)
        {
            global $post;

            if ($post->post_type === SD_POST_TYPE) {

                $custom = get_post_custom($post->ID);
                $sd_actor_country = isset($custom['sd_actor_country'][0]) ? $custom['sd_actor_country'][0] : '';
                $sd_actor_common = isset($custom['sd_actor_common'][0]) ? $custom['sd_actor_common'][0] : '';
                $sd_eye_color = isset($custom['sd_eye_color'][0]) ? $custom['sd_eye_color'][0] : '';
                $sd_actor_hair = isset($custom['sd_actor_hair'][0]) ? $custom['sd_actor_hair'][0] : '';
                $sd_actor_size = isset($custom['sd_actor_size'][0]) ? $custom['sd_actor_size'][0] : '';
                $sd_actor_weight = isset($custom['sd_actor_weight'][0]) ? $custom['sd_actor_weight'][0] : '';
                $sd_actor_instagram = isset($custom['sd_actor_instagram'][0]) ? $custom['sd_actor_instagram'][0] : '';
                $sd_actor_twitter = isset($custom['sd_actor_twitter'][0]) ? $custom['sd_actor_twitter'][0] : '';
                $sd_actor_life = isset($custom['sd_actor_life'][0]) ? $custom['sd_actor_life'][0] : '';
                $sd_post_excerpt = $post->post_excerpt;

                $sd_actor_awards = wp_get_post_terms($post->ID, SD_AWARDS_TAX);
                $sd_actor_movies = wp_get_post_terms($post->ID, SD_MOVIES_TAX);

                $sd_actor_gallery = isset($custom['sd_actor_gallery'][0]) ? $custom['sd_actor_gallery'][0] : '';
                $sd_actor_gallery = json_decode($sd_actor_gallery, false);
                $sd_actor_gallery = gallery_shortcode(array(
                    "ids" => $sd_actor_gallery
                ));


                ob_start();

                include "view/sd_post_content.php";

                $output_string = ob_get_contents();
                ob_end_clean();
                return $output_string;

            } else {
                return $content;
            }
        }


        // sort by id
        function logos_sortable_columns($sortable_columns)
        {
            $sortable_columns['post_id'] = 'post_id';
            return $sortable_columns;
        }


        /**
         * function to add id and image fields to columns list
         * */
        function nwpost_add_id_column($columns, $post_type)
        {
            return ($post_type == SD_POST_TYPE) ?
                array_slice($columns, 0, 1, true) +
                array('post_id' => __('ID', SD_TEXTDOMAIN), 'image_src' => __('Image', SD_TEXTDOMAIN)) +
                array_slice($columns, 1, NULL, true) :
                $columns;
        }

        /**
         * function to add id and image fields value to logo grid
         * */
        function nwpost_id_column_content($column)
        {
            global $post;
            switch ($column) {
                case 'post_id': {
                    edit_post_link("<b>" . $post->ID . "</b>");
                    break;
                }
                case 'image_src':
                    echo "<img width=\"50\" src='" . get_the_post_thumbnail_url() . "'/>";
                    break;
            }
        }


        public function register_actor_post()
        {
            $labels = array(
                'name' => _x('Actors', 'plural form of logo post type', SD_TEXTDOMAIN),
                'singular_name' => _x('Actor', 'singular form of logo post type', SD_TEXTDOMAIN),
                'menu_name' => __('Schauspieler Datenbank', SD_TEXTDOMAIN),
                'name_admin_bar' => __('Partner Network', SD_TEXTDOMAIN),
                'all_items' => __('All Actors', SD_TEXTDOMAIN),
                'add_new' => __('Add New Actor', SD_TEXTDOMAIN),
                'add_new_item' => __('Add New Actor', SD_TEXTDOMAIN),
                'new_item' => __('New Actor', SD_TEXTDOMAIN),
                'edit_item' => __('Edit Actor', SD_TEXTDOMAIN),
                'view_item' => __('View Actor', SD_TEXTDOMAIN),
                'search_items' => __('Search Actor', SD_TEXTDOMAIN),
                'parent_item_colon' => __('Parent Actors:', SD_TEXTDOMAIN),
                'not_found' => __('No Actor found.', SD_TEXTDOMAIN),
                'not_found_in_trash' => __('No Actor found in Trash.', SD_TEXTDOMAIN),
                'title_name' => 'ddd',

                'exclude_from_search' => true,

                'featured_image' => __('Actor Main Photo', SD_TEXTDOMAIN),
                'set_featured_image' => __('Set Actor Photo', SD_TEXTDOMAIN),
                'remove_featured_image' => __('Remove Actor Photo', SD_TEXTDOMAIN),
                'use_featured_image' => __('Use Actor Photo', SD_TEXTDOMAIN),
            );

            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,
                'query_var' => true,
                'rewrite' => true,
                'capability_type' => 'post',
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array('title', 'thumbnail', 'excerpt'),
                'menu_icon' => 'dashicons-images-alt2',
            );

            register_post_type(SD_POST_TYPE, $args);
        }


        public function register_actor_taxonomies()
        {
            // register MOVIE taxonomy-------------------------------
            $movie_labels = array(
                'name' => __("Movies", SD_TEXTDOMAIN),
                'label_name' => __("Movies", SD_TEXTDOMAIN),
                'singular_name' => __("Movies", SD_TEXTDOMAIN),
                'search_items' => __('Select Movies', SD_TEXTDOMAIN),
                'popular_items' => __('Favorite Movies', SD_TEXTDOMAIN),
                'all_items' => __('movies list', SD_TEXTDOMAIN),
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => __('Edit Movie', SD_TEXTDOMAIN),
                'update_item' => __('Refresh Movie', SD_TEXTDOMAIN),
                'add_new_item' => __('Add New Movie', SD_TEXTDOMAIN),
                'new_item_name' => __('New Movie', SD_TEXTDOMAIN),
                'separate_items_with_commas' => __('Separate Movie with commas', SD_TEXTDOMAIN),
                'add_or_remove_items' => __('Add or remove Movie', SD_TEXTDOMAIN),
                'choose_from_most_used' => __('Choose from the most used Movies', SD_TEXTDOMAIN),
                'menu_name' => 'All Movies'
            );

            register_taxonomy(SD_MOVIES_TAX,
                array(SD_POST_TYPE),
                array(
                    'hierarchical' => false,
                    'labels' => $movie_labels,
                    'public' => true,
                    'show_in_nav_menus' => true,
                    'show_ui' => true,
                    'show_tagcloud' => true,
                    'update_count_callback' => '_update_post_term_count',
                    'query_var' => true,
                    'rewrite' => array(
                        'slug' => SD_MOVIES_TAX,
                        'hierarchical' => false
                    ),
                )
            );

            // register AWARDS taxonomy---------------------------------------
            $awards_labels = array(
                'name' => __("Awards", SD_TEXTDOMAIN),
                'label_name' => __("Awards", SD_TEXTDOMAIN),
                'singular_name' => __("Awards", SD_TEXTDOMAIN),
                'search_items' => __('Select Awards', SD_TEXTDOMAIN),
                'popular_items' => __('Favorite Awards', SD_TEXTDOMAIN),
                'all_items' => __('Awards list', SD_TEXTDOMAIN),
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => __('Edit Awards', SD_TEXTDOMAIN),
                'update_item' => __('Refresh Awards', SD_TEXTDOMAIN),
                'add_new_item' => __('Add New Awards', SD_TEXTDOMAIN),
                'new_item_name' => __('New Awards', SD_TEXTDOMAIN),
                'separate_items_with_commas' => __('Separate Awards with commas', SD_TEXTDOMAIN),
                'add_or_remove_items' => __('Add or remove Awards', SD_TEXTDOMAIN),
                'choose_from_most_used' => __('Choose from the most used Awards', SD_TEXTDOMAIN),
                'menu_name' => 'All Awards'
            );

            register_taxonomy(SD_AWARDS_TAX,
                array(SD_POST_TYPE),
                array(
                    'hierarchical' => false,
                    'labels' => $awards_labels,
                    'public' => true,
                    'show_in_nav_menus' => true,
                    'show_ui' => true,
                    'show_tagcloud' => true,
                    'update_count_callback' => '_update_post_term_count',
                    'query_var' => true,
                    'rewrite' => array(
                        'slug' => SD_AWARDS_TAX,
                        'hierarchical' => false
                    ),
                )
            );
        }


        public function add_meta_boxes()
        {
            add_meta_box(
                'sd-gallery',
                __('Photo gallery', SD_TEXTDOMAIN),
                array($this, 'actor_photo_gallery'),
                SD_POST_TYPE,
                'norma',
                'high'
            );


            add_meta_box(
                'sd-actor-info',
                __('Actor info', SD_TEXTDOMAIN),
                array($this, 'actor_info_meta_box'),
                SD_POST_TYPE,
                'side',
                'high'
            );

            add_meta_box(
                'sd-actor-life',
                __('Actor life', SD_TEXTDOMAIN),
                array($this, 'actor_life_meta_box'),
                SD_POST_TYPE,
                'normal',
                'low'
            );
        }


        public function actor_photo_gallery()
        {
            global $post;

            $custom = get_post_custom($post->ID);
            $sd_actor_gallery_str = isset($custom['sd_actor_gallery'][0]) ? $custom['sd_actor_gallery'][0] : '';

            $sd_actor_gallery = json_decode($sd_actor_gallery_str, false);
            $sd_actor_gallery = gallery_shortcode(array(
                "ids" => $sd_actor_gallery
            ));
            ?>

            <p>
                <input type="hidden" value="<?php echo $sd_actor_gallery_str ?>" class="sd_actor_gallery"
                       name="sd_actor_gallery"><br/>
                <a href="#" class="set_custom_images button"><?php _e("Photo Gallery", SD_TEXTDOMAIN) ?></a>
            </p>

            <div id="sd_gallery_images">
                <?php echo $sd_actor_gallery; ?>
            </div>

            <?php
        }


        public function actor_life_meta_box()
        {

            global $post;

            $custom = get_post_custom($post->ID);
            $actor_life = isset($custom['sd_actor_life'][0]) ? $custom['sd_actor_life'][0] : '';

            wp_editor(
                $actor_life,
                'sd_actor_life',
                $settings = array(
                    'textarea_rows' => 8,
                    'media_buttons' => false,
                    'tinymce' => true,
                    'quicktags' => true,
                )
            );
            ?>

            <p class="sslp-note">**Note: HTML is allowed.</p>

            <?php wp_nonce_field('sslp_post_nonce', 'sslp_add_edit_staff_member_noncename'); ?>

            <?php

        }

        public function actor_info_meta_box()
        {

            global $post;

            $custom = get_post_custom($post->ID);
            $sd_actor_country = isset($custom['sd_actor_country'][0]) ? $custom['sd_actor_country'][0] : '';
            $sd_actor_common = isset($custom['sd_actor_common'][0]) ? $custom['sd_actor_common'][0] : '';
            $sd_eye_color = isset($custom['sd_eye_color'][0]) ? $custom['sd_eye_color'][0] : '';
            $sd_actor_hair = isset($custom['sd_actor_hair'][0]) ? $custom['sd_actor_hair'][0] : '';
            $sd_actor_size = isset($custom['sd_actor_size'][0]) ? $custom['sd_actor_size'][0] : '';
            $sd_actor_weight = isset($custom['sd_actor_weight'][0]) ? $custom['sd_actor_weight'][0] : '';
            $sd_actor_instagram = isset($custom['sd_actor_instagram'][0]) ? $custom['sd_actor_instagram'][0] : '';
            $sd_actor_twitter = isset($custom['sd_actor_twitter'][0]) ? $custom['sd_actor_twitter'][0] : '';


            // Add a nonce field so we can check for it later.
            wp_nonce_field('sd_save_meta_box_data', 'sd_meta_box_nonce');

            include "view/sd_actor_info.php";

        }


        public function save_meta_box_data($post_id)
        {
            // verify authorization,

            if (!$this->_nw_security_check($_POST, $post_id)) return;

            update_post_meta(
                $post_id,
                'sd_actor_life',
                isset($_POST['sd_actor_life']) ? $_POST['sd_actor_life'] : ''
            );
            update_post_meta(
                $post_id,
                'sd_actor_twitter',
                isset($_POST['sd_actor_twitter']) ? esc_url_raw(trim($_POST['sd_actor_twitter'])) : ''
            );
            update_post_meta(
                $post_id,
                'sd_actor_instagram',
                isset($_POST['sd_actor_instagram']) ? esc_url_raw(trim($_POST['sd_actor_instagram'])) : ''
            );
            update_post_meta(
                $post_id,
                'sd_actor_weight',
                isset($_POST['sd_actor_weight']) ? (int)sanitize_text_field($_POST['sd_actor_weight']) : ''
            );
            update_post_meta(
                $post_id,
                'sd_actor_size',
                isset($_POST['sd_actor_size']) ? (int)sanitize_text_field($_POST['sd_actor_size']) : ''
            );
            update_post_meta(
                $post_id,
                'sd_actor_hair',
                isset($_POST['sd_actor_hair']) ? sanitize_text_field($_POST['sd_actor_hair']) : ''
            );
            update_post_meta(
                $post_id,
                'sd_eye_color',
                isset($_POST['sd_eye_color']) ? sanitize_text_field($_POST['sd_eye_color']) : ''
            );
            update_post_meta(
                $post_id,
                'sd_actor_common',
                isset($_POST['sd_actor_common']) ? sanitize_text_field($_POST['sd_actor_common']) : ''
            );
            update_post_meta(
                $post_id,
                'sd_actor_country',
                isset($_POST['sd_actor_country']) ? sanitize_text_field($_POST['sd_actor_country']) : ''
            );
            update_post_meta(
                $post_id,
                'sd_actor_gallery',
                isset($_POST['sd_actor_gallery']) ? sanitize_text_field($_POST['sd_actor_gallery']) : ''
            );
        }


        /**
         * It checks if the nonce is valid and if a user is allowed to save the data or if it is an autosave action
         * @param array $post_data It is basically the $_POST value
         * @param int $post_id The id of the current post
         * @access private
         * @return bool It returns true if the checks passes. Otherwise false is returned.
         */
        private function _nw_security_check($post_data, $post_id)
        {
            // checks are divided into 3 parts for readability.
            if (!empty($post_data['nw_meta_box_nonce']) && wp_verify_nonce($post_data['sd_meta_box_nonce'], 'sd_save_meta_box_data')) {
                return true;
            }
            // If this is an autosave, our form has not been submitted, so we don't want to do anything. returns false
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
                return false;
            }
            // Check the user's permissions.
            if (current_user_can('edit_post', $post_id)) {
                return true;
            }
            return false;
        }


    } // ends NW_PosttypeUtility class


} // ends if






