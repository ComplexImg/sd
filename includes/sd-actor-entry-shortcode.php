<?php

/**
 * Protect direct access
 */
if (!defined('ABSPATH')) die('Accessing this file directly is denied.');

if (!class_exists("SD_Actor_Entry_Shortcode")) {
    class SD_Actor_Entry_Shortcode
    {
        public function __construct()
        {
            add_shortcode(SD_ACTOR_ENTRY_TAG, array($this, 'output_shortcode'));
        }

        /**
         * Registers Shortcode
         */
        public function output_shortcode($atts)
        {
            $params = shortcode_atts(array(
                'entry' => null,
            ), $atts);


            $query = array(
                'post_type' => SD_POST_TYPE,
                'posts_per_page' => -1,
            );

            if ($params['entry'] !== '') {
                $query = array_merge(
                    $query,
                    array(
                        'post__in' => explode(",", $params['entry'])
                    )
                );
            }


            $actors = new WP_Query($query);

            global $post;

            ob_start();

            while ($actors->have_posts()) :
                $actors->the_post();
                $link = get_permalink($post->ID);
                $img_url = get_the_post_thumbnail_url();
                $name = get_the_title();
                ?>
                <div class="sd_actor_entry">
                    <a href="<?php echo $link ?>">
                        <img src="<?php echo $img_url ?>">
                        <?php echo $name ?>
                    </a>
                </div>
                <?php

            endwhile;
            wp_reset_postdata();

            $output_string = ob_get_contents();
            ob_end_clean();
            return $output_string;

        } // end class NW_Shortcode
    }
} // end if


